'''
Created on 2018/06/06

@author: naoki_okada

共通設定など定数リスト
'''
import os,sys

class Common():
    def __init__(self):
        __path = os.path.normpath(os.path.join(
            os.path.dirname(os.path.abspath(__file__)),'../'))
        #root のディレクトリ取得
        self.__root_dir = __path
        #data のディレクトリ取得
        self.__data_dir = os.path.join(self.__root_dir,'data')
        #config のディレクトリ取得
        self.__config_dir = os.path.join(self.__root_dir,'config')
        #bin のディレクトリ取得
        self.__bin_dir = os.path.join(self.__root_dir,'bin')

    '''  root_dir プロパティ  '''
    def get_root_dir(self):
        return self.__root_dir

    root_dir = property(get_root_dir)

    '''  data_dir プロパティ  '''
    def get_data_dir(self):
        return self.__data_dir

    data_dir = property(get_data_dir)

    '''  config_dir プロパティ  '''
    def get_config_dir(self):
        return self.__config_dir

    config_dir = property(get_config_dir)

    '''  bin_dir プロパティ  '''
    def get_bin_dir(self):
        return self.__bin_dir

    bin_dir = property(get_bin_dir)