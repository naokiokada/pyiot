'''
Created on 2018/06/06

@author: naoki_okada

メッセージを作る
simple factoryパターン
'''

from app.twitter import Twitter
from app.line import Line

'''  メッセージング処理する各apiサービス  '''
class MsgSimpleFactory():
    def create(self, service):
        if service in {'twitter'}:
            return Twitter()

        elif service in {'mail'}:
            pass

        elif service in {'slack'}:
            pass

        elif service in {'line'}:
            return Line()
