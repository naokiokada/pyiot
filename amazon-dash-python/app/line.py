'''
Created on 2018/06/15

@author: naoki_okada

Line投稿クラス
'''
import requests,json
from .abs_message import AbsMessage

class Line(AbsMessage):
    def __init__(self):
        self._user_id = ""
        self._message = ""
        self._to      = ""
        self._channel_access_token = None
        self._headers = {}
        self._request = None

    def set_to(self, value):
        self._to = value

    def set_body(self, value):
        self._message = value
        data = {
            'to' : self._user_id,
            'messages' : [
                {
                    'type' : 'text',
                    'text' : self._message
                }
            ]
        }
        jsonstr = json.dumps(data)
        self._message = jsonstr

    def set_from(self):
        pass

    def send(self):
        try:
            self._request = requests.post(self._to, data=self._message, headers=self._headers)
            if(self._request.status_code == 200):
                return True
            else:
                print ("Error: %d" % self._request.status_code)
                raise
        except:
            print('line post error!!')
            raise

    ''' ヘッダーをセットする '''
    def set_headers(self, key, value):
        self._headers[key] = value

    def create_request(self):
        requests.post(self._to, self._message, self._)

    '''  user_idのプロパティ  '''
    def _get_user_id(self):
        return self._user_id

    def _set_user_id(self, value):
        self._user_id = value

    def _del_user_id(self, value):
        del self._user_id

    user_id = property(_get_user_id, _set_user_id, _del_user_id)

    '''  channel_access_tokenのプロパティ  '''
    def _get_channel_access_token(self):
        return self._channel_access_token

    def _set_channel_access_token(self, value):
        self._channel_access_token = value

    def _del_channel_access_token(self, value):
        del self._channel_access_token