'''
Created on 2018/06/06

@author: naoki_okada

ツイッターapiクラス
'''
import json
from requests_oauthlib import OAuth1Session
from .abs_message import AbsMessage

class Twitter(AbsMessage):
    def __init__(self):
        self._consumer_key        = None
        self._consumer_secret     = None
        self._access_token        = None
        self._access_token_secret = None
        self._post_param          = {}
        self._api_url             = None
        self._twitter             = None

    '''  各apiキーのプロパティ  '''
    def _get_consumer_key(self):
        return self._consumer_key

    def _set_consumer_key(self, value):
        self._consumer_key = value

    def _del_consumer_key(self):
        del self._consumer_key

    consumer_key = property(_get_consumer_key, _set_consumer_key,
                             _del_consumer_key, "Property consumer_key")

    '''  consumer_secretのプロパティ  '''
    def _get_consumer_secret(self):
        return self._consumer_secret

    def _set_consumer_secret(self, value):
        self._consumer_secret = value

    def _del_consumer_secret(self):
        del self._consumer_secret

    consumer_secret = property(_get_consumer_secret, _set_consumer_secret,
                             _del_consumer_secret, "Property consumer_secret")

    '''  access tokenのプロパティ  '''
    def _get_access_token(self):
        return self._access_token

    def _set_access_token(self, value):
        self._access_token = value

    def _del_access_token(self):
        del self._access_token

    access_token = property(_get_access_token, _set_access_token,
                             _del_access_token, "Property access_token")

    '''  access token secretプロパティ  '''
    def _get_access_token_secret(self):
        return self._access_token_secret

    def _set_access_token_secret(self, value):
        self._access_token_secret = value

    def _del_access_token_secret(self):
        del self._access_token_secret

    access_token_secret = property(_get_access_token_secret, _set_access_token_secret,
                             _del_access_token_secret, "Property consumer_key")


    def set_from(self):
        pass

    def set_to(self, value):
        self._api_url = value

    def set_cc(self, value):
        pass

    def set_bcc(self, value):
        pass

    def set_title(self):
        pass

    '''  twitter postパラメーターを作る  '''
    def set_body(self, value):
        self._post_param["status"] = value

    '''  twitterオブジェクトを作る  '''
    def create_twitter(self):
        self._twitter = OAuth1Session(self._consumer_key,
                                self._consumer_secret,
                                self._access_token,
                                self._access_token_secret)

    '''  twitterにpostする  '''
    def send(self):
        if(not self._twitter):
            self.create_twitter()
        try:
            req = self._twitter.post(self._api_url, params = self._post_param)
            if(req.status_code == 200):
                return True
            else:
                print ("Error: %d" % req.status_code)
                raise
        except:
            print("twitter post error!!")
            raise