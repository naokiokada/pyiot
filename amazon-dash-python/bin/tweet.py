#! /usr/bin/python
# -*- coding: utf-8 -*-

"""
pythonツイートプログラム
"""
import os
import configparser
from datetime import datetime

#configの読み込み
#現在の時間を取得（yyyymmddhhmmss形式）

#保存ファイルをオープンして、書き込む
#ボタンを押された回数を数える
#ツイート内容を作る
#ツイートを行う。

def count_push_byfile(file):
    pass

def do_tweet():
    pass

"""
dash buttonが押された日時を保存する。
"""
def write_push_time(time, file):
    pass



if __name__ == '__main__':
    #実際の処理
    INI_FILE = '../config/twitter.ini';
    pathname = os.path.dirname(os.path.abspath(__name__))
    pathname = os.path.join(pathname, INI_FILE)
    pathname = os.path.normpath(pathname)
    #設定ファイル読み込み
    config = configparser.ConfigParser()
    config.read(pathname, 'UTF-8')
    #config.get('settings', ConsumerKey)

    #現在の日時
    nowtime = datetime.now().strftime("%Y/%m/%d %H:%m:%s")
