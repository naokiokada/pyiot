#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os,sys
from datetime import datetime

MESSAGE = '''こんにちは！！
'''

'''  ボタンの押されたログをカウントする  '''
def log_file_count(log_file):
    count = 0
    if os.path.exists(log_file):
        for line in open(log_file):
            line = line.strip()
            if line:
                count+=1
    else:
        #からファイル
        pass

    return count

'''  ボタンが押されたログを記録する  '''
def write_log_file(log_file, now_time):
    if os.path.exists(log_file):
        file = open(log_file, 'a')
        file.write(now_time)
        file.close()
    else:
        file = open(log_file, 'w')
        file.write(now_time)
        file.close()

if __name__ == '__main__':
    '''  appendの読み込み  '''
    path = os.path.normpath(os.path.join(
    os.path.dirname(os.path.abspath(__file__)),'../'))
    sys.path.append(path)
    from app import common
    from app import msg_simple_factory as factory
    from app import config_ini

    '''  共通情報の取得  '''
    cm = common.Common()
    pathname = cm.config_dir
    '''  configファイルの指定  '''
    pathname = os.path.join(pathname, 'system.ini')
    config   = config_ini.ConfigIni()
    config.ini_file = pathname

   #現在の日時算出
    now_time = datetime.now().strftime("%Y/%m/%d %H:%M:%S  dash button pushed\n")

    #log_fileの値が必要
    log_file = config.get_config_value_bykey(
        'data_settings', 'LINE_LOG')
    log_dir = cm.data_dir
    log_file = os.path.join(log_dir, log_file)
    count = log_file_count(log_file)
    count+=1

    #ログにボタンが押された日時を書きこむ
    write_log_file(log_file, now_time)

    #tweet内容を作る
    count = str(count)
    '''  lineオブジェクトの取得  '''
    msg = factory.MsgSimpleFactory()
    line = msg.create('line')
    line.set_to(config.get_config_value_bykey(
        'line_settings','API_URL')
    #本文を設定する
    #twitterに送る
    if line.send():
        print('tweet成功')
    else:
        print('tweet失敗')
