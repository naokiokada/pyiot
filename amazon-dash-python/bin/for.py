#!/usr/bin/env python
# -*- coding: utf-8 -*-

#list = ["a","b","c"]
#for one in list:
#    print(one)
    #1行ずつ表示するには、改行を入れる。
    #print("\n")

#list_val = [1,2,"a","b"]
#dict_val = {"name":"okada","age":40}
#print(list_val[0])
#print(dict_val["name"])

#a = 6
#if (a > 10):
#    print("10より大きい")
#elif(a > 5):
#    print("5より大きい")
#else:
#    print("それ以外")

def function_abc(num):
    num = num + 1
    return num
a = 10
a = function_abc(a)
print(str(a))